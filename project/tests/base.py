# project/tests/base.py



# imports
from flask_testing import TestCase
from project       import create_app
from project       import db



# create the app
app = create_app()



# basic behaviour of app in test
class BaseTestCase( TestCase ):
    def create_app( self ):
        app.config.from_object( 'project.config.TestingConfig' )
        return app

    def setUp( self ):
        db.create_all()
        db.session.commit()

    def tearDown( self ):
        db.session.remove()
        db.drop_all()
